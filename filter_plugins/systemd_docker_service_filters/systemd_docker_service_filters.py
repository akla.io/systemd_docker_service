from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase
from ansible.utils.listify import listify_lookup_plugin_terms


def get_linked_services(docker_compose, **kwargs):
    #linked services will be in network mode or volumes_from
    try:
        project_name = kwargs['project_name']
        service_name = kwargs['service_name']
        instance_id = kwargs['instance_id']

        service_to_container_name={}
        project_name_sanitized = sanitize_names_for_docker(project_name)

        for service_key, service_values in docker_compose['services'].iteritems():
            if 'container_name' in service_values:
                container_name = service_values['container_name']
            else:
                container_name = project_name_sanitized + '_' + sanitize_names_for_docker(service_key) + '_' + instance_id

            service_to_container_name[service_key] = container_name

        linked_service_container_names = set()

        service = docker_compose['services'][service_name]

        if 'volumes_from' in service:
            print service['volumes_from']
            for linked_service in service['volumes_from']:
                if linked_service.startswith('container'):
                    linked_service_container_names.add(linked_service.split(':')[1])
                else:
                    linked_service_name = linked_service.split(':')[0]
                    linked_service_container_names.add(service_to_container_name[linked_service_name])

            #container case
        if 'network_mode' in service:
            if service['network_mode'].startswith('service'):

                linked_service_name = service['network_mode'].split(':')[1]
                linked_service_container_names.add(service_to_container_name[linked_service_name])

            elif service['network_mode'].startswith('container'):
                linked_service_container_names.add(service['network_mode'].split(':')[1])

        return list(linked_service_container_names)

    except Exception as e:
        raise AnsibleError(e)

def sanitize_names_for_docker(name):
    return name.replace('_', '').replace('-', '')

class FilterModule(object):
    def filters(self):
        return {
            'get_linked_services': get_linked_services,
            'sanitize_names_for_docker': sanitize_names_for_docker
        }
