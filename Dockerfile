FROM registry.gitlab.com/docker.akla.io/fedora-systemd-ansible:master.2016-10-11-2016.b120b6d

WORKDIR /code

RUN mkdir -p /code/roles/systemd_docker_service

COPY ./ /code/roles/systemd_docker_service/

RUN for f in /code/roles/systemd_docker_service/tests/*; do ln -s $f; done
