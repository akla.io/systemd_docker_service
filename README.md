systemd_docker_service
=========

This role takes a docker-compose.yml for docker-compose version 2.0 and writes 
out systemd service files for Dockerized services. 

For more details about constructing a 
docker-compose.yml, see the [Docker Compose version 2 file reference](https://docs.docker.com/compose/compose-file/compose-file-v2/#compose-and-docker-compatibility-matrix)

Requirements
------------

This role should only be run on targets that use the systemd init system. 

Currently, it only supports docker-compose version 2.0. Future releases will add
support for docker-compose 3.0. 

Role Variables
--------------
This role has two variables that need to be defined: 


`systemd_docker_service_instance_id`: an id number for distinguishing between services. This allows you to start multiple services using the same docker-compose

`systemd_docker_service_composefile_path`: the path to the docker-compose.yml defining the services you want to launch


Example docker-compose.yml which launches an
[Elasticsearch](https://www.elastic.co/products/elasticsearch) service and 
a [Kibana](https://www.elastic.co/products/kibana) service:
```
---
version: '2.0'
services:
  elasticsearch:
    container_name: elasticsearch
    network_mode: "service:kibana"
    image: docker.elastic.co/elasticsearch/elasticsearch:6.1.1
    ports:
      - "9200:9200"
    volumes:
      - "/srv/tag/elasticsearch/data:/usr/share/elasticsearch/data"
  kibana:
    image: docker.elastic.co/kibana/kibana:6.1.1
    ports:
      - "5601:5601"
    volumes_from:
      - container:systemddockerservice_elasticsearch_1
      - elasticsearch
    environment:
      ELASTICSEARCH_URL: http://localhost:9200
      VIRTUAL_HOST: kibana.akla.io
      LETSENCRYPT_HOST: kibana.akla.io
      LETSENCRYPT_EMAIL: admin@akashiclabs.com

```



Example Playbook
--------------

```
---
- hosts: localhost
  vars:
    systemd_docker_service_instance_id: '1'
    systemd_docker_service_composefile_path: '/code/roles/systemd_docker_service/tests/docker-compose.yml'
  roles:
    - systemd_docker_service

```


License
-------

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Author Information
------------------

This role was created by Rachel Shadoan of Akashic Labs (www.akashiclabs.com).
If you have questions or suggestions about this role, please contact
opensource@akashiclabs.com. Impolite missives will be summarily ignored.
